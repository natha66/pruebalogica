//
//  ViewController.swift
//  PruebaLogica
//
//  Created by macbook on 29/11/20.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var nTextField: UITextField!
    @IBOutlet weak var mTextField: UITextField!
    @IBOutlet weak var startButtonOutlet: UIButton!
    @IBOutlet weak var lblReply: UILabel!
    var isStarted: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStyle()
    }
    func setStyle() {}
    func startButtonView() {
        self.isStarted = !self.isStarted
        self.startButtonOutlet.backgroundColor = self.isStarted ? .red : .systemGreen
        self.startButtonOutlet.setTitle(self.isStarted ? "Corriendo" : "Empezar", for: .normal)
    }
    @IBAction func startButton(_ sender: UIButton) {
        self.startButtonView()
        self.start()
    }
    func start() {
        guard let nString = nTextField.text, let mString = mTextField.text else { return messaggeAlert(message: "No se detecto el campo N o M") }
        guard let nNumber = Int(nString), let mNumber = Int(mString) else { return messaggeAlert(message: "Se necesita un numero entero") }
        if nNumber > mNumber {
            if mNumber % 2 == 0 {
                self.lblReply.text = "Up"
            } else {
                self.lblReply.text = "Down"
            }
        } else {
            if nNumber % 2 == 0 {
                self.lblReply.text = "Left"
            } else {
                self.lblReply.text = "Right"
            }
        }
        self.startButtonView()
    }
    func messaggeAlert(message: String) {
        self.startButtonView()
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
}

